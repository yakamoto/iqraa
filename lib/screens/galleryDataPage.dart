import 'dart:typed_data';
import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:dio/dio.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:wallpaperplugin/wallpaperplugin.dart';
class GalleryDataPage extends StatefulWidget {
  String keyword;

  GalleryDataPage(this.keyword);

  @override
  _GalleryDataPageState createState() => _GalleryDataPageState();
}

class _GalleryDataPageState extends State<GalleryDataPage> {
  int currentPage = 1;
  int size = 10;
  int sizeImages = 0;
  int totalPages;
  ScrollController _scrollController = new ScrollController();
  List<dynamic> images = [];
  var galleryData;

  bool downloading = false;
  var progressString = "";
  double progress = 0.0;
  String imageData;
  bool dataLoaded = false;
  String _wallpaperStatus = 'Initial';
  Map<PermissionGroup, PermissionStatus> permissions;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPermission();
    getData();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (currentPage < totalPages) {
          ++currentPage;
          getData();
        }
      }
    });



  }
  void getPermission() async {
    permissions = await PermissionHandler().requestPermissions([
      //PermissionGroup.location,
      //PermissionGroup.camera,
      //PermissionGroup.locationAlways,
      //PermissionGroup.phone,
      //PermissionGroup.sensors,
      PermissionGroup.storage,
      //PermissionGroup.microphone,
    ]);


  }
  getData() {
    String url =
        "https://pixabay.com/api/?key=9378151-585a70f9f3cc191a00b4534b0&q=${widget.keyword}&page=${currentPage}&per_page=${size}&image_type=photo&pretty=true";
    http.get(url).then((resp) {
      setState(() {
        galleryData = json.decode(resp.body);
        images.addAll(galleryData['hits']);
        sizeImages = galleryData['totalHits'];
        if (galleryData['totalHits'] % size == 0) {
          totalPages = galleryData['totalHits'] ~/ size;
        } else
          totalPages = (galleryData['totalHits'] / size).floor() + 1;
      });
    }).catchError((onError) {
      print(onError);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('${sizeImages} images')),
        body: downloading
            ? Center(
              child: Container(
          height: 300.0,
          width: 300.0,
          child: LiquidCircularProgressIndicator(
            value: progress, // Defaults to 0.5.
            valueColor: AlwaysStoppedAnimation(Colors.amber), // Defaults to the current Theme's accentColor.
            backgroundColor: Colors.white, // Defaults to the current Theme's backgroundColor.
            borderColor: Colors.blue,
            borderWidth: 5.0,
            direction: Axis.vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right). Defaults to Axis.vertical.
            center: Text("Loading..."+progressString),
          ),
        ),
            )
            : (galleryData == null
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                controller: _scrollController,
                itemCount: (galleryData == null ? 0 : images.length),
                itemBuilder: (context, index) {
                  return Column(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Card(
                          child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Center(
                                child: Text(
                                  "#"+images[index]['tags'].toString().replaceAll(', ', " #"),
                              style: TextStyle(
                                  fontSize: 22,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )),
                          ),
                          color: Colors.green,
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Card(
                            child: Image.network(
                              images[index]['webformatURL'],
                              fit: BoxFit.cover,
                              loadingBuilder: (BuildContext context,
                                  Widget child,
                                  ImageChunkEvent loadingProgress) {
                                if (loadingProgress == null) return child;
                                return Center(
                                  child: CircularProgressIndicator(
                                    value: loadingProgress.expectedTotalBytes !=
                                            null
                                        ? loadingProgress
                                                .cumulativeBytesLoaded /
                                            loadingProgress.expectedTotalBytes
                                        : null,
                                  ),
                                );
                              },
                            ),
                          )),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Card(
                          child: ButtonBar(
                            children: <Widget>[
                              FlatButton(
                                child: const Icon(Icons.wallpaper),
                                onPressed: () {
                                    setWallpaper(images[index]['largeImageURL']);
                                },
                                color: Colors.blue,
                              ),
                              FlatButton(
                                child: const Icon(Icons.share),
                                onPressed: () async {
                                  await _shareImageFromUrl(images[index]['largeImageURL']);
                                },
                                color: Colors.blue,
                              ),
                              FlatButton(
                                child: const Icon(Icons.file_download),
                                onPressed: () {


                                    _downloadAndSavePhoto(images[index]['largeImageURL']);
                                  print('$dataLoaded');
                                },
                                color: Colors.blue,
                              ),
                            ],
                            alignment: MainAxisAlignment.center,
                          ),
                        ),
                        color: Colors.green,
                      ),
                    ],
                  );
                })));
  }
  void showDownloadProgress(received, total) {
    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + "%");
    }
  }
  _downloadAndSavePhoto(url) async {

    print("download method");

    Dio dio = Dio();

    try {
      var dir = await getExternalStorageDirectory();

      var response = await dio.download(url, "${dir.path}"+'/Iqraa/images/'+new DateTime.now().toString()+'.jpg',
          onReceiveProgress: (rec, total) {
            print("Rec: $rec , Total: $total");

            setState(() {
              downloading = true;
              progress = (rec / total) * 100;
              progressString = ((rec / total) * 100).toStringAsFixed(0) + "%";
            });
          });
    } catch (e) {
      print(e);
    }

    setState(() {
      downloading = false;
      progressString = "Completed";
      dataLoaded = true;
    });
    print("Download completed");
  }

  Future<void> _shareImageFromUrl(url) async {
    try {
      var request = await HttpClient().getUrl(Uri.parse(
          url));
      var response = await request.close();
      Uint8List bytes = await consolidateHttpClientResponseBytes(response);
      await Share.file('ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg');
    } catch (e) {
      print('error: $e');
    }
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController.dispose();
  }
  Future<void> setWallpaper(url) async {
    String wallpaperStatus = 'Unexpected Result';
   // if(await _checkAndGetPermission()!=null) {
      final String _localFile = await _downloadFileUsingDio(
          url,
          new DateTime.now().toString());
      // Platform messages may fail, so we use a try/catch PlatformException.
      try {
        Wallpaperplugin.setWallpaperWithCrop(localFile: _localFile);
        wallpaperStatus = 'new Wallpaper set';
      } on PlatformException {
        print('Platform exception');
        wallpaperStatus = 'Platform Error Occured';
  //    }
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted)
      return;
    setState(() {
      _wallpaperStatus = wallpaperStatus;
    });
  }
  ///This code helps in downloading a jpeg to local folder
  Future<String> _downloadFileUsingDio(String url, String _photoId) async {
    final Dio dio = Dio();
    final String dir = await _localPath;
    final String localFile = '$dir/$_photoId.jpeg';
    final File file = File(localFile);
    if (!file.existsSync()) {
      try {
        await dio.download(url, localFile, onReceiveProgress: (int received, int total) {
          if (total != -1) {
            print('Photo downloading : ' +
                (received / total * 100).toStringAsFixed(0) +
                '%');
          }
        });
        return localFile;
      } on PlatformException catch (error) {
        print(error);
      }
    }
    return localFile;
  }
  ///returns the local path + /wallpapers, this is where the image file is downloaded.
  Future<String> get _localPath async {
    final Directory appDocDirectory = await getExternalStorageDirectory();
    final Directory directory =
    await Directory(appDocDirectory.path + '/wallpapers')
        .create(recursive: true);
    // The created directory is returned as a Future.
    return directory.path;
  }


  /// This method checks for permission and if not given will request the user for the same.
  /// It will return true if permission is given, or else will return null
  static Future<bool> _checkAndGetPermission() async{
    final PermissionStatus permission =
    await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
    if (permission != PermissionStatus.granted) {
      final Map<PermissionGroup, PermissionStatus> permissions =
      await PermissionHandler().requestPermissions(<PermissionGroup>[PermissionGroup.storage]);
      if(permissions[PermissionGroup.storage] != PermissionStatus.granted){
        return null;
      }
    }
    return true;
  }

}
