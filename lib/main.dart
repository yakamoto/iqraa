import 'package:flutter/material.dart';
import 'package:iqraa/screens/galleryDataPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Photos',
      theme: ThemeData(

        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Photos'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  void _incrementCounter() {
    setState(() {
      _counter++;
      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryDataPage(keyword)));
      textEditingController.text="";
    });
  }
  String keyword="";

  TextEditingController textEditingController=new TextEditingController();
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(child: TextField(
              style: TextStyle(fontSize: 22),
              onChanged: (value){
                setState(() {
                  this.keyword=value;
                });
              },
              controller: textEditingController,
              decoration: InputDecoration(hintText: 'Tape a '+widget.title),
              onSubmitted: (value){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryDataPage(value)));
                textEditingController.text="";
              },
            ),padding:EdgeInsets.all(10)),
            /*Container(width:double.infinity,child: RaisedButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryDataPage(keyword)));
                textEditingController.text="";
              },
              color: Colors.green,
              child: Text("Get Dogs",style: TextStyle(fontSize:22,color: Colors.white),),
            ),padding:EdgeInsets.all(10)),
            _counter>0?Icon(Icons.favorite,color: Colors.pink):Text(''),*/
            /*Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),*/
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.search),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}
